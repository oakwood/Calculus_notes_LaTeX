#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
this script performs simple plot
"""


import numpy as np 

import matplotlib.pyplot as plt


import os   #this is to save the files

relativeDir = os.path.dirname(__file__)  
		# relativeDir is the path where this python script file is
		
# dataset = datafilename = "modulator_40GHz_data_20151031" 



# figsDir = os.path.join(relativeDir, "figs/")		
# 		# figsDir is the subfolder where I want the plot files saved
# 
# if not os.path.exists(figsDir):
#         os.makedirs(figsDir)	


####
# define the function
####

def sinf(x, intensity, period, phase, ):
	return (intensity/2) * np.cos(  (x / period)  * (2 * np.pi) ) 
	
def parabf(x, a, b ):
	return a *  x**2 + b 

parameters = [100, 20, 4]


fig, ax = plt.subplots()

# ax.yaxis.set_major_locator(majorLocator) # applies distance between (major) ticks


# plot commands

Xarray = np.linspace(0, 10, 1000)



####
# plotting 
####

ax.plot(Xarray, sinf(Xarray, 1, 4, 0.5), label="f(x)") 



ax.plot(Xarray, parabf(Xarray, 0.05, 0.1,), label="g(x)") 

ax.plot(Xarray, sinf(Xarray, 1, 4, 0.5) + parabf(Xarray, 0.05, 0.1,) , label="f(x) + g(x)") 


ax.plot(Xarray, 2 * sinf(Xarray, 1, 4, 0.5) + 0.5 * parabf(Xarray, 0.05, 0.1,), label="2 * f(x) + 0.5 * g(x)") 


plt.xlabel('x')
plt.ylabel('y')



# plt.suptitle(dataset, y=0.95)  # title for the whole figure, with repositioning
	#alternative method for title


ax.set_title("linear combination", fontsize=20)




annotation_string = r"""The function plotted is:
$f(x) \ = \ \frac{{I}}{{2}} \cos\left(2 \pi \ \frac{{x}}{{T}}\right)$ 

where:
$I = ${0}
$T = ${1}""".format(parameters[0], parameters[1])

#plt.annotate(annotation_string, xy=(0.05, 0.60), xycoords='axes fraction',                                        backgroundcolor='w', fontsize=14)
# this is to have a textbox in the plot with the parameters from the fit


#		# note: if a ".format()" command is used
#		# you need to enclose LaTeX arguments in
#		# double curly brackets, so not to conflict with ".format()" {#} targets




plt.legend()


###
# just plot on screen
###



#plt.show()


###
# save plot files in a directory
###

#the trick with the path here is to make the output files to 
# be created in a "relative path"


fileName = "lincomb.png"
filePath = os.path.join(relativeDir, fileName)

plt.savefig(filePath, bbox_inches=0)

# NB



